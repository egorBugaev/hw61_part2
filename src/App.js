import React, {Component} from 'react';
import Header from "./components/Header/Header";
import Table from "./components/Table/Table";
import Modal from "./components/UI/modal/modal";
import Button from "./components/UI/Button/Button";
import fire from './firebase';

class App extends Component {
  state = {
    modalShow: false,
    data: [],
    item: {
      id: '',
      status: '',
      order: '',
      date: '',
      supplier: '',
      respName: '',
      orderConfirm: '',
      typeOfProduct: '',
      circulation: '',
      dateOff: '',
      currentState: '',
      option: '',
      actuallyConsumedMaterial: '',
      clientData: '',
      manager: '',
      paperType: ''
    }
  };

  componentWillMount() {
    /* Create reference to data in Firebase Database */
    let messagesRef = fire.database().ref('data').orderByKey().limitToLast(100);
    messagesRef.on('child_added', snapshot => {
      /* Update React state when message is added at Firebase Database */
      let message = snapshot.val();
      this.setState({data: [message].concat(this.state.data)});
    })
  }

  addMessage(e) {
    e.preventDefault();
    let item = {...this.state.item};
    item.id = Date.now();
    fire.database().ref('data').push(item);
    this.modalHide();
  }

  changeStatus = (event) => {
    let item = {...this.state.item};
    item.status = event.target.value;
    this.setState({item});
  };
  changeOrder = (event) => {
    let item = {...this.state.item};
    item.order = event.target.value;
    this.setState({item});
  };
  changeDate = (event) => {
    let item = {...this.state.item};
    item.date = event.target.value;
    this.setState({item});
  };
  changeSupplier = (event) => {
    let item = {...this.state.item};
    item.supplier = event.target.value;
    this.setState({item});
  };
  changeRespName = (event) => {
    let item = {...this.state.item};
    item.respName = event.target.value;
    this.setState({item});
  };
  changeOrderConfirm = (event) => {
    let item = {...this.state.item};
    item.orderConfirm = event.target.value;
    this.setState({item});
  };
  changeCirculation = (event) => {
    let item = {...this.state.item};
    item.circulation = event.target.value;
    this.setState({item});
  };
  changeTypeOfProduct = (event) => {
    let item = {...this.state.item};
    item.typeOfProduct = event.target.value;
    this.setState({item});
  };
  changeDateOff = (event) => {
    let item = {...this.state.item};
    item.dateOff = event.target.value;
    this.setState({item});
  };
  changeCurrentState = (event) => {
    let item = {...this.state.item};
    item.currentState = event.target.value;
    this.setState({item});
  };
  changeOption = (event) => {
    let item = {...this.state.item};
    item.option = event.target.value;
    this.setState({item});
  };
  changeActuallyConsumedMaterial = (event) => {
    let item = {...this.state.item};
    item.actuallyConsumedMaterial = event.target.value;
    this.setState({item});
  };
  changeClientData = (event) => {
    let item = {...this.state.item};
    item.clientData = event.target.value;
    this.setState({item});
  };
  changeManager = (event) => {
    let item = {...this.state.item};
    item.manager = event.target.value;
    this.setState({item});
  };
  changePaperType = (event) => {
    let item = {...this.state.item};
    item.paperType = event.target.value;
    this.setState({item});
  };

  modalShow = () => {
    this.setState({modalShow: true});
  };
  modalHide = () => {
    this.setState({modalShow: false});
  };

  render() {
    console.log(this.state.data);
    return <div className="App">
      <Header
        add={this.modalShow}/>

      <Modal
        onSubmit={this.addItem}
        // ref={ el => this.inputEl = el }
        closed={this.modalHide}
        show={this.state.modalShow}
        title="Add task"
        changeStatus={(event) => this.changeStatus(event)}
        changeOrder={(event) => this.changeOrder(event)}
        changeDate={(event) => this.changeDate(event)}
        changeSupplier={(event) => this.changeSupplier(event)}
        changeRespName={(event) => this.changeRespName(event)}
        changeOrderConfirm={(event) => this.changeOrderConfirm(event)}
        changeTypeOfProduct={(event) => this.changeTypeOfProduct(event)}
        changeCirculation={(event) => this.changeCirculation(event)}
        changeDateOff={(event) => this.changeDateOff(event)}
        changeCurrentState={(event) => this.changeCurrentState(event)}
        changeOption={(event) => this.changeOption(event)}
        changeActuallyConsumedMaterial={(event) => this.changeActuallyConsumedMaterial(event)}
        changeClientData={(event) => this.changeClientData(event)}
        changeManager={(event) => this.changeManager(event)}
        changePaperType={(event) => this.changePaperType(event)}
      >
        <Button btnType="Danger" clicked={this.modalHide}>CANCEL</Button>
        <Button btnType="Success" clicked={this.addMessage.bind(this)}>CONTINUE</Button>
      </Modal>
      <Table
        data={this.state.data}/>
    </div>;
  }
}

export default App;
