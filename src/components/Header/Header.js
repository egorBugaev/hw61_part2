import React from 'react';
import Moment from 'react-moment';
import './header.css'
import logo from './logo.png'
    const Header=props=>{
        let date = new Date();
            date = date.toDateString();

        return <header>
            <img className="logo" src={logo} alt="logo"/>
          <Moment format='DD MMM YYYY, HH:mm' date={props.datetime}/>
            <button onClick={props.add} className="add">Add</button>

        </header>
        
    };

export default Header;