import React from 'react';
import ReactTable from 'react-table'
import './table.css'

const Table = props => {


        const columns = [
            {   Header: 'Статус',
                accessor: 'status' // String-based value accessors!
            },
            {   Header: 'Заказ наряд',
                accessor: 'order' // String-based value accessors!
            },
            {   Header: 'Дата приема заказа ',
                accessor: 'date' // String-based value accessors!
            },
            {   Header: 'ЗАКАЗЧИК',
                accessor: 'supplier' // String-based value accessors!
            },
            {   Header: 'ОТВЕТСТВЕННЫЙ ЗА ЗАКАЗ ВРЕМЯ ОТПРАВКИ НА ПЕЧАТЬ',
                accessor: 'respName' // String-based value accessors!
            },
            {   Header: 'ПОДТВЕРЖДЕНИЕ ЗАКАЗА ',
                accessor: 'orderConfirm' // String-based value accessors!
            },
            {   Header: 'НАИМЕНОВАНИЕ ПРОДУКЦИИ ',
                accessor: 'typeOfProduct' // String-based value accessors!
            },
            {   Header: 'ТИРАЖ',
                accessor: 'circulation' // String-based value accessors!
            },
            {   Header: 'ДАТА СДАЧИ ЗАКАЗА (КЛИЕНТУ ) ',
                accessor: 'dateOff' // String-based value accessors!
            },
            {   Header: 'ТЕКУЩЕЕ СОСТОЯНИЕ ЗАКАЗА  ',
                accessor: 'currentState' // String-based value accessors!
            },
            {   Header: 'Опции ',
                accessor: 'option' // String-based value accessors!
            },
            {   Header: 'Бумага тип, формат, граммаж, кол-во',
                accessor: 'paperType' // String-based value accessors!
            },
            {   Header: 'Фактически израсходовано материала ',
                accessor: 'actuallyConsumedMaterial' // String-based value accessors!
            },
            {   Header: 'ФИО ЗАКАЗЧИКА  № ТЕЛЕФОНА ',
                accessor: 'clientData' // String-based value accessors!
            },
            {   Header: 'Мэнеджер ',
                accessor: 'manager' // String-based value accessors!
            },

            ];

     return(       <ReactTable
        data={props.data}
        columns={columns}
        />
     )

};

export default Table;