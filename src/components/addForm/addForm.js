import React from 'react';
// import DatePicker from 'react-datepicker';
// import moment from 'moment';
// import 'react-datepicker/dist/react-datepicker.css';
//
// class Example extends React.Component {
//   constructor (props) {
//     super(props)
//     this.state = {
//       startDate: moment()
//     };
//     this.handleChange = this.handleChange.bind(this);
//   }
//
//   handleChange(date) {
//     this.setState({
//       startDate: date
//     });
//   }
//
//   render() {
//     return <DatePicker
//       selected={this.state.startDate}
//       onChange={this.handleChange}
//     />;
//   }
// }

const Form = props=>(
    <form onSubmit={props.onSubmit}>
      <ul>
        <li>Статус<input onChange={props.changeStatus} type="text"/></li>
        <li>Заказ наряд<input onChange={props.changeOrder}  type="text"/></li>
        <li>Дата приема заказа <input onChange={props.changeDate} type="text"/></li>
        <li>ЗАКАЗЧИК<input onChange={props.changeSupplier} type="text"/></li>
        <li>ОТВЕТСТВЕННЫЙ ЗА ЗАКАЗ ВРЕМЯ ОТПРАВКИ НА ПЕЧАТЬ<input onChange={props.changeRespName} type="text"/></li>
        <li>ПОДТВЕРЖДЕНИЕ ЗАКАЗА <input onChange={props.changeOrderConfirm} type="text"/></li>
        <li>НАИМЕНОВАНИЕ ПРОДУКЦИИ <input onChange={props.changeTypeOfProduct} type="text"/></li>
        <li>ТИРАЖ<input onChange={props.changeCirculation} type="text"/></li>
        <li>ДАТА СДАЧИ ЗАКАЗА (КЛИЕНТУ )<input onChange={props.changeDateOff}  type="text"/></li>
        <li>ТЕКУЩЕЕ СОСТОЯНИЕ ЗАКАЗА <input onChange={props.changeCurrentState}  type="text"/></li>
        <li>Опции<input onChange={props.changeOption} type="text"/></li>
        <li>Бумага тип, формат, граммаж, кол-во<input onChange={props.changePaperType} type="text"/></li>
        <li>Фактически израсходовано материала<input onChange={props.changeActuallyConsumedMaterial} type="text"/></li>
        <li>ФИО ЗАКАЗЧИКА № ТЕЛЕФОНА<input onChange={props.changeClientData} type="text"/></li>
        <li>Мэнеджер<input onChange={props.changeManager} type="text"/></li>
      </ul>
    </form>


    );

export default Form