import React from 'react';
import './modal.css'
import Wrapper from "../../../hoc/Wrapper";
import Backdrop from "../BackDrop/BackDrop";
import Form from "../../addForm/addForm";

const Modal = props=>(
    <Wrapper>
    <Backdrop show={props.show} clicked={props.closed}/>
    <div className='Modal' style={{display: props.show ? 'block':'none'}}/*style={{
        transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
        opacity: props.show ? '1' : '0'

    }}*/>
     <Form
       changeStatus={props.changeStatus}
       changeOrder={props.changeOrder}
       changeDate={props.changeDate}
       changeSupplier={props.changeSupplier}
       changeRespName={props.changeRespName}
       changeOrderConfirm={props.changeOrderConfirm}
       changeTypeOfProduct={props.changeTypeOfProduct}
       changeCirculation={props.changeCirculation}
       changeDateOff={props.changeDateOff}
       changeCurrentState={props.changeCurrentState}
       changeOption={props.changeOption}
       changeActuallyConsumedMaterial={props.changeActuallyConsumedMaterial}
       changeClientData={props.changeClientData}
       changeManager={props.changeManager}
       changePaperType={props.changePaperType}
     />
      {props.children}
    </div>
    </Wrapper>
);

export default Modal